//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "bash ~/.local/bin/blocks-cvc/mcout.sh",           300,	 0},
	{"", "bash ~/.local/bin/blocks-cvc/mcruns.sh",          60,	     0},
	{"", "~/.local/bin/blocks-cvc/arch_updates.sh",         5,		 0},
	{"", "bash ~/.local/bin/blocks-cvc/mail.sh",            5,		 0},
	{"", "~/.local/bin/blocks-cvc/bright.sh",               0,		 9},
	{"", "~/.local/bin/blocks-cvc/volume.sh",               0,		10},
	{"", "~/.local/bin/blocks-cvc/cpuspace.sh",             5,		 0},
	{"", "~/.local/bin/blocks-cvc/ram.sh",				    5,		 0},
	{"", "~/.local/bin/blocks-cvc/time.sh",			        30,		 0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "|";
static unsigned int delimLen = 5;
